#pragma once

class Ring
{
private:

	friend class Tower;

	//Ring's value. Can be considered as its size
	int value;

	//A pointer to the next Ring in the Tower stack
	Ring* next;

public:
	Ring(const int& value, Ring* nextRing);
	Ring();
	~Ring();

	int GetValue() const;
	void SetNext(Ring* nextRing);
	Ring* GetNext() const;
};
