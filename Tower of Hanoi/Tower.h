#pragma once

class Tower
{
private:
	//A pointer to the top of the stack
	class Ring* top;

public:
	Tower();
	~Tower();

	void Push(Ring* newRing);
	void Pop();
	Ring* GetTop();

	//Last moved ring
	static int lastMoved;
};

