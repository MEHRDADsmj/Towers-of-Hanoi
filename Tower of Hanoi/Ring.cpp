#include "Ring.h"

Ring::Ring(const int& initValue, Ring* nextRing) : value(initValue), next(nextRing)
{}

Ring::Ring() : value(0), next(nullptr)
{}

Ring::~Ring()
{}

int Ring::GetValue() const
{
	return value;
}

void Ring::SetNext(Ring* nextRing)
{
	next = nextRing;
}

Ring* Ring::GetNext() const
{
	return next;
}