#include <iostream>
#include "Tower.h"
#include "Ring.h"
#define TOWERS_COUNT 3

using namespace std;

#pragma region Globals

//Used for the first IsSolved condition in RecursiveSolve function
bool tempBool = false;

#pragma endregion

#pragma region Function Prototypes

//Returns true if the problem is solved. which means 2 out of 3 of  towers are empty
bool IsSolved(Tower* towers);

//Inserts all of the rings of the first tower
void Initialize(const int& numberOfRings, Tower* towers);

//Solves the problem using loops
void LoopSolve(Tower* towers);

//Solves the problem using recursive function
void RecursiveSolve(Tower* towers, int& currentTowerIndex, int& nextTowerIndex);

//It moves the rings of each tower to the next towers as many times as possible
void MoveRings(Tower* towers);

//Moves the current tower's ring to the selected tower and logs it to the screen
void Transfer(Tower* towers, const int& currentTowerIndex, const int& nextTowerIndex);

#pragma endregion

int main()
{
	int numberOfRings;
	system("color 0A");
	cout << "Enter the number of rings: ";
	cin >> numberOfRings;
	cout << endl;
	int firstTowerIndex = 0, secondTowerIndex = 1;
	Tower towers[TOWERS_COUNT];
	Initialize(numberOfRings, towers);
	//LoopSolve(towers);
	RecursiveSolve(towers, firstTowerIndex, secondTowerIndex);
	system("pause");
	return 0;
}

#pragma region Function Implementations

bool IsSolved(Tower* towers)
{
	return (towers[0].GetTop() == nullptr && towers[1].GetTop() == nullptr) ||
		(towers[1].GetTop() == nullptr && towers[2].GetTop() == nullptr) ||
		(towers[0].GetTop() == nullptr && towers[2].GetTop() == nullptr);
}

void Initialize(const int& numberOfRings, Tower * towers)
{
	for (int num = numberOfRings; num > 0; num--)
	{
		Ring* ring = new Ring(num, towers[0].GetTop());
		towers[0].Push(ring);
	}
	Tower::lastMoved = 0;
}

void LoopSolve(Tower * towers)
{
	while (true)
	{
		return MoveRings(towers);
	}
}

void RecursiveSolve(Tower * towers, int& currentTowerIndex, int& nextTowerIndex)
{
	if (IsSolved(towers) && tempBool)
	{
		return;
	}
	else
	{
		tempBool = true;
		if (towers[currentTowerIndex].GetTop() != nullptr &&
			towers[currentTowerIndex].GetTop()->GetValue() != Tower::lastMoved)
		{
			if (towers[nextTowerIndex].GetTop() == nullptr ||
				towers[currentTowerIndex].GetTop()->GetValue() < towers[nextTowerIndex].GetTop()->GetValue())
			{
				Transfer(towers, currentTowerIndex, nextTowerIndex);
				RecursiveSolve(towers, currentTowerIndex, nextTowerIndex);
			}
			else if (towers[(nextTowerIndex + 1) % TOWERS_COUNT].GetTop() == nullptr ||
				towers[currentTowerIndex].GetTop()->GetValue() < towers[(nextTowerIndex + 1) % TOWERS_COUNT].GetTop()->GetValue())
			{
				Transfer(towers, currentTowerIndex, (nextTowerIndex + 1) % TOWERS_COUNT);
				RecursiveSolve(towers, currentTowerIndex, nextTowerIndex);
			}
			else
			{
				currentTowerIndex++;
				currentTowerIndex %= TOWERS_COUNT;
				nextTowerIndex = (currentTowerIndex + 1) % TOWERS_COUNT;
				RecursiveSolve(towers, currentTowerIndex, nextTowerIndex);
			}
		}
		else
		{
			currentTowerIndex++;
			currentTowerIndex %= TOWERS_COUNT;
			nextTowerIndex++;
			nextTowerIndex %= TOWERS_COUNT;
			RecursiveSolve(towers, currentTowerIndex, nextTowerIndex);
		}
	}
}

void MoveRings(Tower * towers)
{
	for (int currentTowerIndex = 0; true; currentTowerIndex++, currentTowerIndex %= TOWERS_COUNT)
	{
		for (int nextTowerIndex = currentTowerIndex + 1;
			//The loop hasn't done a whole lap
			nextTowerIndex != currentTowerIndex &&
			//Current tower is not empty
			towers[currentTowerIndex].GetTop() != nullptr &&
			//Its ring is not the one that moved on the last move
			towers[currentTowerIndex].GetTop()->GetValue() != Tower::lastMoved;
			nextTowerIndex++, nextTowerIndex %= TOWERS_COUNT)
		{
			nextTowerIndex %= TOWERS_COUNT;
			if (towers[nextTowerIndex].GetTop() == nullptr ||
				towers[currentTowerIndex].GetTop()->GetValue() < towers[nextTowerIndex].GetTop()->GetValue())
			{
				Transfer(towers, currentTowerIndex, nextTowerIndex);
				if (IsSolved(towers))
				{
					return;
				}
			}
		}
	}
}

void Transfer(Tower * towers, const int& currentTowerIndex, const int& nextTowerIndex)
{
	Ring* tempRing = new Ring(towers[currentTowerIndex].GetTop()->GetValue(), towers[currentTowerIndex].GetTop()->GetNext());
	towers[nextTowerIndex].Push(tempRing);
	cout << "Ring with value: " << towers[currentTowerIndex].GetTop()->GetValue()
		<< " to tower: " << nextTowerIndex + 1 << endl << endl;
	towers[currentTowerIndex].Pop();
	tempRing = nullptr;
}

#pragma endregion
