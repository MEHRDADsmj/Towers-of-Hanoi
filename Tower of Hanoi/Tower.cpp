#include "Tower.h"
#include "Ring.h"

using namespace std;

//Assigning static member
int Tower::lastMoved = 0;

Tower::Tower() : top(nullptr)
{}

Tower::~Tower()
{}

void Tower::Push(Ring* newRing)
{
	newRing->next = top;
	top = newRing;
	lastMoved = newRing->GetValue();
}

void Tower::Pop()
{
	top = top->next;
}

Ring* Tower::GetTop()
{
	return top;
}
